// ������� DLL-����.
#include "stdafx.h"
#include "QuadInterpolate.h"
#include<string>
#include "Windows.h"
#include <fstream>
#include <vector>
using namespace std;

void Interpolate(std::string n1, std::string n2, std::string output) {

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	private class Polynom
	{
	private: double a;
	private: double b;
	private: double c;

	public: Polynom(Point fir, Point sec, Point last)
		{
			a = (last.y - fir.y) / ((last.x - fir.x) * (last.x - sec.x)) - (sec.y - fir.y) / ((sec.x - fir.x) * (last.x - sec.x));
			b = (sec.y - fir.y) / (sec.x - fir.x) - a * (sec.x + fir.x);
			c = fir.y - b * fir.x - a * fir.x * fir.x;
		}

	public: double GetValue(double val)
		{
			return a * val * val + b * val + c;
		}
	};

	private class Segment
	{

	private: double start;
	private: double finish;

	public: bool Contains(double c)
		{
			if ((c >= start) && (c <= finish))
			{
				return true;
			}
			return false;
		}

	public: Polynom *function;

	public: Segment(Point a, Point b, Point c)
		{
			start = a.x;
			finish = c.x;
			function = new Polynom(a, b, c);
		}

	public: double Calculate(double va)
		{
			return (*function).GetValue(va);
		}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;
		secondName = n2;
		outputName = output + ".txt";

		std::ifstream reader1(firstName);
		std::ifstream reader2(secondName);
		std::ofstream writer(outputName);

		long long len;
		long long lenReq;

		reader1 >> len;
		reader2 >> lenReq;

		std::vector<double> requipments;
		std::vector<Point*> points;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			points.push_back(new Point(a, b));

		}

		for (int i = 0; i < lenReq; i++)
		{
			reader2 >> a;
			requipments.push_back(a);
		}

		std::vector<Segment*> segments;

		for (int i = 0; i < len - 2; i++)
		{
			segments.push_back(new Segment(*points[i], *points[i + 1], *points[i + 2]));
		}


		writer << lenReq << '\n';

		for (int i = 0; i < lenReq; i++)
		{
			for (int j = 0; j < len - 2; j++)
			{
				if ((*segments[j]).Contains(requipments[i]))
				{
					double result = (*segments[j]).Calculate(requipments[i]);
					writer << requipments[i] << " " << result << '\n';
					break;
					break;
				}
			}
		}

		writer.close();
		reader1.close();
		reader2.close();

	}

}

double InterpolateTime(std::string n1, double value) {

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	private class Polynom
	{
	private: double a;
	private: double b;
	private: double c;

	public: Polynom(Point fir, Point sec, Point last)
	{
		a = (last.y - fir.y) / ((last.x - fir.x) * (last.x - sec.x)) - (sec.y - fir.y) / ((sec.x - fir.x) * (last.x - sec.x));
		b = (sec.y - fir.y) / (sec.x - fir.x) - a * (sec.x + fir.x);
		c = fir.y - b * fir.x - a * fir.x * fir.x;
	}

	public: double GetValue(double val)
	{
		return a * val * val + b * val + c;
	}
	};

	private class Segment
	{

	private: double start;
	private: double finish;

	public: bool Contains(double c)
	{
		if ((c >= start) && (c <= finish))
		{
			return true;
		}
		return false;
	}

	public: Polynom *function;

	public: Segment(Point a, Point b, Point c)
	{
		start = a.x;
		finish = c.x;
		function = new Polynom(a, b, c);
	}

	public: double Calculate(double va)
	{
		return (*function).GetValue(va);
	}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;

		std::ifstream reader1(firstName);

		long long len;

		reader1 >> len;

		std::vector<double> requipments;
		std::vector<Point*> points;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			points.push_back(new Point(a, b));

		}

		std::vector<Segment*> segments;

		for (int i = 0; i < len - 2; i++)
		{
			segments.push_back(new Segment(*points[i], *points[i + 1], *points[i + 2]));
		}
		double result = 0;
			for (int j = 0; j < len - 2; j++)
			{
				if ((*segments[j]).Contains(value))
				{
					result = (*segments[j]).Calculate(value);
				}
			}

		reader1.close();
		return result;
	}

}

