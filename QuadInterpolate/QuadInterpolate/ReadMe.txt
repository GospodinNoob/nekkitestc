========================================================================
    DYNAMIC LINK LIBRARY : QuadInterpolate Project Overview
========================================================================

AppWizard has created this QuadInterpolate DLL for you.  

This file contains a summary of what you will find in each of the files that
make up your QuadInterpolate application.

QuadInterpolate.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

QuadInterpolate.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

QuadInterpolate.cpp
    This is the main DLL source file.

QuadInterpolate.h
    This file contains a class declaration.

AssemblyInfo.cpp
	Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
