#pragma once
#include<string>
#include <msclr\marshal_cppstd.h>
#include <fstream>
#include <vector>

namespace Problem1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::RadioButton^  radioButton1;
	private: System::Windows::Forms::RadioButton^  radioButton2;
	private: System::Windows::Forms::RadioButton^  radioButton3;
	private: System::Windows::Forms::RadioButton^  radioButton4;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(325, 104);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"OK";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(257, 12);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(75, 17);
			this->radioButton1->TabIndex = 1;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"��������";
			this->radioButton1->UseVisualStyleBackColor = true;
			this->radioButton1->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton1_CheckedChanged);
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(257, 35);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(96, 17);
			this->radioButton2->TabIndex = 2;
			this->radioButton2->Text = L"������������";
			this->radioButton2->UseVisualStyleBackColor = true;
			this->radioButton2->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton2_CheckedChanged);
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(257, 58);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(151, 17);
			this->radioButton3->TabIndex = 3;
			this->radioButton3->Text = L"����������� ���������";
			this->radioButton3->UseVisualStyleBackColor = true;
			this->radioButton3->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton3_CheckedChanged);
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Location = System::Drawing::Point(257, 81);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(80, 17);
			this->radioButton4->TabIndex = 4;
			this->radioButton4->Text = L"��� �����";
			this->radioButton4->UseVisualStyleBackColor = true;
			this->radioButton4->CheckedChanged += gcnew System::EventHandler(this, &MyForm::radioButton4_CheckedChanged);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(13, 13);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 5;
			this->textBox1->Text = L"a.txt";
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(13, 40);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 6;
			this->textBox2->Text = L"b.txt";
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox2_TextChanged);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(151, 81);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 7;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox3_TextChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(119, 16);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(76, 13);
			this->label1->TabIndex = 8;
			this->label1->Text = L"������ ����";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(119, 43);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(72, 13);
			this->label2->TabIndex = 9;
			this->label2->Text = L"������ ����";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(52, 88);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(96, 13);
			this->label3->TabIndex = 10;
			this->label3->Text = L"���� ����������";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(10, 114);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(121, 13);
			this->label4->TabIndex = 11;
			this->label4->Text = L"��� ��������� �����";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(412, 140);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->radioButton4);
			this->Controls->Add(this->radioButton3);
			this->Controls->Add(this->radioButton2);
			this->Controls->Add(this->radioButton1);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"Problem1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}

	public: int inetrpolateType;

	public: String^ firstFile;
	public: String^ secondFile;
	public: String^ dllFile;

	public: HINSTANCE dll;

	typedef void(__stdcall *INTERPOLATE)(std::string, std::string, std::string);
	typedef double(__stdcall *INTERPOLATETIME)(std::string, double val);
		
	INTERPOLATE IntFunc;
	INTERPOLATETIME TimeFunc;


public: double func(double x) {
	return log(x);
}

#pragma endregion
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	msclr::interop::marshal_context context;
	std::wstring dllFileName = context.marshal_as<std::wstring>(textBox3->Text);
	std::string firstFileName = context.marshal_as<std::string>(textBox1->Text);
	std::string secondFileName = context.marshal_as<std::string>(textBox2->Text);
	std::string name = "";

	std::wstring linearName = L"LinearInterpolate.dll";
	std::wstring quadName = L"QuadInterpolate.dll";
	std::wstring splineCubeName = L"CubeSplineInterpolate.dll";
	if (inetrpolateType == 0) {
		name = "Linear";
		dll = LoadLibrary(linearName.c_str());
	}
	if (inetrpolateType == 1) {
		name = "Quad";
		dll = LoadLibrary(quadName.c_str());
	}
	if (inetrpolateType == 2) {
		name = "CubeSpline";
		dll = LoadLibrary(splineCubeName.c_str());
	}
	if (inetrpolateType == 3) {
		name = "Custom";
		dll = LoadLibrary(dllFileName.c_str());
	}
	if (dll != NULL){
		
		/*firstFileName = "ln(x)in" + name +".txt";
		secondFileName = "ln(x)out" + name +".txt";

		std::ofstream in(firstFileName);
		std::ofstream out(secondFileName);

		double start = 0.51;
		double cur = start;
		int step = 0;
		int maxStep = 100;
		in << maxStep << '\n';
		while (step < maxStep) {
			in << cur << ' ' << func(cur) << '\n';
			step++;
			cur += 0.1;
		}

		maxStep = 100;
		//out << cur << ' ' << start << '\n';
		out << maxStep << '\n';
		for (int i = 0; i < maxStep; i++) {
			double r = rand() / (double)RAND_MAX;
			r *= (cur - start);
			r += start;
			out << r << '\n';
		}

		in.close();
		out.close();*/

		IntFunc = (INTERPOLATE)GetProcAddress(dll, "Interpolate");
		IntFunc(firstFileName, secondFileName, firstFileName + secondFileName + name);
		/*TimeFunc = (INTERPOLATETIME)GetProcAddress(dll, "InterpolateTime");
		double result = TimeFunc(firstFileName, 0.5);*/
		FreeLibrary(dll);
		label4->Text = gcnew String((firstFileName + secondFileName + name).c_str());

		/*std::ofstream testWriter("Test" + name + ".txt");
		testWriter << result;
		testWriter.close();*/

		/*double sumError = 0;
		double sumAbs = 0;
		double sqareError = 0;
		std::ifstream answerReader(firstFileName + secondFileName + name + ".txt");
		std::ofstream logWriter(firstFileName + secondFileName + name + "Log.txt");
		int cou;
		double a, b;
		answerReader >> cou;
		logWriter << cou << '\n';
		for (int i = 0; i < cou; i++) {
			answerReader >> a >> b;
			double real = func(a);
			double est = real - b;
			logWriter << a << ' ' << est << '\n';
			sumError += est;
			sumAbs += abs(est);
			sqareError += est*est;
		}
		logWriter << sumError << ' ' << sumAbs << '\n' << sqrt(sqareError / cou);
		logWriter.close();
		answerReader.close();*/
	}
}
private: System::Void radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	inetrpolateType = 0;
}
private: System::Void radioButton2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	inetrpolateType = 1;
}
private: System::Void radioButton3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	inetrpolateType = 2;
}
private: System::Void radioButton4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	inetrpolateType = 3;
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
};
}
