#pragma once
#include<string>
#include <msclr\marshal_cppstd.h>
#include <fstream>
#include <vector>
#include <map>

namespace Problem2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>


public class Vertex {
	public: std::string parentName;
	public:	double localValue;
	public:	double globalValue;
	public:	bool needUpdate;

	public: Vertex(double val){
		localValue = val;
		parentName = "";
		needUpdate = false;
		globalValue = val;
	}

};

	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected:
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label4;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(197, 99);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"OK";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(13, 13);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->Text = L"a.txt";
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(12, 39);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 2;
			this->textBox2->Text = L"b.txt";
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox2_TextChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 109);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(121, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"��� ��������� �����";
			this->label1->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(119, 20);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(87, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"���� ��������";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(118, 46);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(112, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"���� ������������";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(13, 66);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 6;
			this->textBox3->Text = L"0.5";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(120, 72);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(40, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"�����";
			this->label4->Click += gcnew System::EventHandler(this, &MyForm::label4_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 130);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"Problem2";
			this->ResumeLayout(false);
			this->PerformLayout();

		}

		typedef double(__stdcall *INTERPOLATETIME)(std::string, double val);

		INTERPOLATETIME TimeFunc;
		public: HINSTANCE dll;

				public: double Update(std::map<std::string, Vertex*> &name, Vertex* ver) {
					if (!(ver->needUpdate)) {
						return ver->globalValue;
					}
					else {
						ver->globalValue = ver->localValue + Update(name, name[ver->parentName]);
						ver->needUpdate = false;
						return ver->globalValue;
					}
				}
#pragma endregion
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {


	std::map<std::string, Vertex*> dictionary;

	msclr::interop::marshal_context context;
	std::string firstFileName = context.marshal_as<std::string>(textBox1->Text);
	std::string secondFileName = context.marshal_as<std::string>(textBox2->Text);
	std::string curFileName = "";

	std::string dotName = "";
	std::string interpolationName = "";
	std::wstring dllName = L"";
	int n = 0;

	std::ifstream intReader(firstFileName);
	std::ifstream dotReader(secondFileName);
	std::ofstream writer(firstFileName + secondFileName + "Output.txt");

	intReader >> n;
	std::string s = "";

	for (int i = 0; i < n; i++) {

		dotName = "";
		interpolationName = "";
		dllName = L"";

		intReader >> dotName >> interpolationName >> s;

		for (size_t i = 0; i < s.size(); i++) {
			dllName += s[i];
		}

		//writer << dotName << ' ' << interpolationName << ' ' << s << '\n';
		dll = LoadLibrary(dllName.c_str());

		TimeFunc = (INTERPOLATETIME)GetProcAddress(dll, "InterpolateTime");
		double result = TimeFunc(interpolationName, std::stod(context.marshal_as<std::string>(textBox3->Text)));
		FreeLibrary(dll);

		dictionary.insert(std::make_pair(dotName, new Vertex(result)));

		//writer << dotName << ' ' << result << '\n';


		label1->Text = gcnew String((firstFileName + secondFileName + "Output.txt").c_str());
	}

	writer << n << '\n';

	dotReader >> n;

	std::string parent = "";

	for(int i = 0; i < n; i++){
		dotReader >> parent >> dotName >> dotName;
		dictionary[dotName]->parentName = parent;
		dictionary[dotName]->needUpdate = true;
	}

	for (auto it = dictionary.begin(); it != dictionary.end(); ++it)
	{
		//writer << (*it).second->needUpdate << '\n';
		Update(dictionary, (*it).second);
	}

	for (auto it = dictionary.begin(); it != dictionary.end(); ++it)
	{
		writer << (*it).first << ' ' << (*it).second->globalValue << '\n';
	}

	intReader.close();
	dotReader.close();
	writer.close();

}
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label4_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
}
};
}
