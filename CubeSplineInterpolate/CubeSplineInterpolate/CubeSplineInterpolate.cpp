// ������� DLL-����.

#include "stdafx.h"
#include "CubeSplineInterpolate.h"
#include "stdafx.h"
#include<string>
#include "Windows.h"
#include <fstream>
#include <vector>
using namespace std;

void Interpolate(std::string n1, std::string n2, std::string output) {

	struct Spline
	{
		double a, b, c, d, x;

	public: Spline(double i, double j) {
		a = i;
		x = j;
	}
	};

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;
		secondName = n2;
		outputName = output + ".txt";

		std::ifstream reader1(firstName);
		std::ifstream reader2(secondName);
		std::ofstream writer(outputName);

		long long len;
		long long lenReq;

		reader1 >> len;
		reader2 >> lenReq;

		std::vector<double> requipments;
		std::vector<double> x, y;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			x.push_back(a);
			y.push_back(b);

		}

		for (int i = 0; i < lenReq; i++)
		{
			reader2 >> a;
			requipments.push_back(a);
		}

		vector<Spline*> splines;
		for (std::size_t i = 0; i < len; ++i)
		{
			splines.push_back(new Spline(y[i], x[i]));
		}
		(*splines[0]).c = 0.0;

		double *alpha = new double[len - 1];
		double *beta = new double[len - 1];
		double A, B, C, F, h_i, h_i1, z;
		alpha[0] = beta[0] = 0.;
		for (std::size_t i = 1; i < len - 1; ++i)
		{
			h_i = x[i] - x[i - 1], h_i1 = x[i + 1] - x[i];
			A = h_i;
			C = 2. * (h_i + h_i1);
			B = h_i1;
			F = 6. * ((y[i + 1] - y[i]) / h_i1 - (y[i] - y[i - 1]) / h_i);
			z = (A * alpha[i - 1] + C);
			alpha[i] = -B / z;
			beta[i] = (F - A * beta[i - 1]) / z;
		}

		(*splines[len - 1]).c = (F - A * beta[len - 2]) / (C + A * alpha[len - 2]);

		for (std::size_t i = len - 2; i > 0; --i)
			(*splines[i]).c = alpha[i] * (*splines[i + 1]).c + beta[i];

		delete[] beta;
		delete[] alpha;

		writer << lenReq << '\n';

		for (std::size_t i = len - 1; i > 0; --i)
		{
			double h_i = x[i] - x[i - 1];
			(*splines[i]).d = ((*splines[i]).c - (*splines[i - 1]).c) / h_i;
			(*splines[i]).b = h_i * (2.0 * (*splines[i]).c + (*splines[i - 1]).c) / 6.0 + (y[i] - y[i - 1]) / h_i;
		}
		for (int i = 0; i < lenReq; i++) {
			std::size_t l = 0, r = len - 1;
			while (l + 1 < r)
			{
				std::size_t k = l + (r - l) / 2;
				if (requipments[i] <= (*splines[k]).x)
					r = k;
				else
					l = k;
			}

			double dx = (requipments[i] - splines[r]->x);
			writer << requipments[i] << ' ' << splines[r]->a + (splines[r]->b + (splines[r]->c / 2. + splines[r]->d * dx / 6.) * dx) * dx << '\n';
		}

		writer.close();
		reader1.close();
		reader2.close();

	}

}

double InterpolateTime(std::string n1, double value) {

	struct Spline
	{
		double a, b, c, d, x;

	public: Spline(double i, double j) {
		a = i;
		x = j;
	}
	};

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;

		std::ifstream reader1(firstName);

		long long len;

		reader1 >> len;

		std::vector<double> requipments;
		std::vector<double> x, y;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			x.push_back(a);
			y.push_back(b);

		}

		vector<Spline*> splines;
		for (std::size_t i = 0; i < len; ++i)
		{
			splines.push_back(new Spline(y[i], x[i]));
		}
		(*splines[0]).c = 0.0;

		double *alpha = new double[len - 1];
		double *beta = new double[len - 1];
		double A, B, C, F, h_i, h_i1, z;
		alpha[0] = beta[0] = 0.;
		for (std::size_t i = 1; i < len - 1; ++i)
		{
			h_i = x[i] - x[i - 1], h_i1 = x[i + 1] - x[i];
			A = h_i;
			C = 2. * (h_i + h_i1);
			B = h_i1;
			F = 6. * ((y[i + 1] - y[i]) / h_i1 - (y[i] - y[i - 1]) / h_i);
			z = (A * alpha[i - 1] + C);
			alpha[i] = -B / z;
			beta[i] = (F - A * beta[i - 1]) / z;
		}

		(*splines[len - 1]).c = (F - A * beta[len - 2]) / (C + A * alpha[len - 2]);

		for (std::size_t i = len - 2; i > 0; --i)
			(*splines[i]).c = alpha[i] * (*splines[i + 1]).c + beta[i];

		delete[] beta;
		delete[] alpha;

		for (std::size_t i = len - 1; i > 0; --i)
		{
			double h_i = x[i] - x[i - 1];
			(*splines[i]).d = ((*splines[i]).c - (*splines[i - 1]).c) / h_i;
			(*splines[i]).b = h_i * (2.0 * (*splines[i]).c + (*splines[i - 1]).c) / 6.0 + (y[i] - y[i - 1]) / h_i;
		}
		double result = 0;
			std::size_t l = 0, r = len - 1;
			while (l + 1 < r)
			{
				std::size_t k = l + (r - l) / 2;
				if (value <= (*splines[k]).x)
					r = k;
				else
					l = k;
			}

			double dx = (value - splines[r]->x);
			result = splines[r]->a + (splines[r]->b + (splines[r]->c / 2. + splines[r]->d * dx / 6.) * dx) * dx;

		reader1.close();
		return result;
	}

}

