#include<string>
#include "Windows.h"
#include "Interpolate.h"
#include <fstream>
#include <vector>
using namespace std;



void Interpolate(std::string n1, std::string n2, std::string output) {

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	class Polynom
	{
	private: double a;
	private: double b;
	private: double c;

	public: Polynom(Point fir, Point sec)
	{
		a = fir.y - sec.y;
		b = sec.x - fir.x;
		c = fir.x * sec.y - sec.x * fir.y;
	}

	public: double GetValue(double val)
	{
		return (-c - a * val) / b;
	}
	};

	class Segment
	{

	private: double start;
	private: double finish;

	public: bool Contains(double c)
	{
		if ((c >= start) && (c <= finish))
		{
			return true;
		}
		return false;
	}

	public: Polynom* function;

	public: Segment(Point a, Point b)
	{
		start = a.x;
		finish = b.x;
		function = new Polynom(a, b);
	}

	public: double Calculate(double va)
	{
		return (*function).GetValue(va);
	}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;
		secondName = n2;
		outputName = output + ".txt";

		std::ifstream reader1(firstName);
		std::ifstream reader2(secondName);
		std::ofstream writer(outputName);

		long long len;
		long long lenReq;

		reader1 >> len;
		reader2 >> lenReq;

		std::vector<double> requipments;
		std::vector<Point*> points;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			points.push_back(new Point(a, b));

		}

		for (int i = 0; i < lenReq; i++)
		{
			reader2 >> a;
			requipments.push_back(a);
		}

		std::vector<Segment*> segments;

		for (int i = 0; i < len - 1; i++)
		{
			segments.push_back(new Segment((*points[i]), *points[i + 1]));
		}

		writer << lenReq << '\n';

		for (int i = 0; i < lenReq; i++)
		{
			for (int j = 0; j < len - 1; j++)
			{
				if ((*segments[j]).Contains(requipments[i]))
				{
					double result = (*segments[j]).Calculate(requipments[i]);
					writer << requipments[i] << " " << result << '\n';
					break;
				}
			}
		}

		writer.close();
		reader1.close();
		reader2.close();
	}

}

double InterpolateTime(std::string n1, double value) {

	class Point
	{

	public: double x;
	public: double y;

	public: Point(double a, double b)
	{
		x = a;
		y = b;
	}

	};

	class Polynom
	{
	private: double a;
	private: double b;
	private: double c;

	public: Polynom(Point fir, Point sec)
	{
		a = fir.y - sec.y;
		b = sec.x - fir.x;
		c = fir.x * sec.y - sec.x * fir.y;
	}

	public: double GetValue(double val)
	{
		return (-c - a * val) / b;
	}
	};

	class Segment
	{

	private: double start;
	private: double finish;

	public: bool Contains(double c)
	{
		if ((c >= start) && (c <= finish))
		{
			return true;
		}
		return false;
	}

	public: Polynom* function;

	public: Segment(Point a, Point b)
	{
		start = a.x;
		finish = b.x;
		function = new Polynom(a, b);
	}

	public: double Calculate(double va)
	{
		return (*function).GetValue(va);
	}

	};

	std::string firstName = "";
	std::string secondName = "";
	std::string outputName = "";

	{
		firstName = n1;

		std::ifstream reader1(firstName);

		long long len;

		reader1 >> len;

		std::vector<double> requipments;
		std::vector<Point*> points;

		double a, b;

		for (int i = 0; i < len; i++)
		{
			reader1 >> a >> b;
			points.push_back(new Point(a, b));

		}

		std::vector<Segment*> segments;

		for (int i = 0; i < len - 1; i++)
		{
			segments.push_back(new Segment((*points[i]), *points[i + 1]));
		}
		double result;
			for (int j = 0; j < len - 1; j++)
			{
				if ((*segments[j]).Contains(value))
				{
					result = (*segments[j]).Calculate(value);
					break;
				}
			}

		reader1.close();
		return result;
	}

}